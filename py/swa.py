import time

import requests
"""
Basic API for getting SWA Data
"""

ENDPOINT = 'https://southwestwifi.com'


def get_fp3d_status():
    epoch_ms = round(time.time() * 1000)
    resp = requests.get(f'{ENDPOINT}/fp3d_logs/last?_={epoch_ms}')
    if not resp.ok:
        return {}
    return resp.json()


def get_status():
    resp = requests.get(f'{ENDPOINT}/current.json')
    if not resp.ok:
        return {}
    return resp.json()
