import time

import swa

# schemas for csv
STATUS_SCHEMA = ('actime24', 'ttgc', 'etad', 'dtzone', 'lat', 'lon', 'gspdVal',
                 'altVal', 'pcent_flt_complete', 'dist_remain')
FP3D_SCHEMA = ('altitudeFeet', 'pitch', 'roll', 'groundSpeedKnots',
               'trueHeading', 'presentLat', 'presentLon', 'trueHeading',
               'distanceToDestinationNauticalMiles', 'presentPhase', 'time',
               'totalFlightTime', 'timeSinceTakeoff', 'timeToDestination',
               'estimatedTimeOfArrival')


def term_statusline(*args):
    """
    write to terminal to repeatedly overwrite the same line

    :param fp: file handle to stdout
    :param line: line to write
    """
    print(*args, end='', flush=True)
    print('\r', end='')


def write_dict_csv(fp, d, keys):
    for key in keys:
        fp.write(f'{d.get(key)}')
        fp.write(',')
    # overwrite the last comma
    # fp.seek(-2)
    fp.write('\n')


def write_csv(fp, *args):
    """
    Write a line to an open csv file

    :param fp: open file handle
    :param args: however many cells you wish to write
    """
    for a in args:
        fp.write(a)
        fp.write(',')
    # overwrite the last comma
    # fp.seek(-1)
    fp.write('\n')


def main():

    init_info = swa.get_fp3d_status()
    if init_info is {}:
        print('Could not connect to southwest\'s systems... are you online?')
        exit(1)
    print(
        f'You appear to be on {init_info.get("tailNumber")}',
        f'from {init_info.get("departureId")} to {init_info.get("destinationId")}',
        f'(Flight {init_info.get("flightNumber")})')

    flight_id = init_info['flightNumber'] + '_' + init_info['tailNumber']
    fp_fp3d_raw = open(f'fp3d_{flight_id}.txt', 'w')
    fp_fp3d_csv = open(f'fp3d_{flight_id}.csv', 'w')
    fp_status_raw = open(f'status_{flight_id}.txt', 'w')
    fp_status_csv = open(f'status_{flight_id}.csv', 'w')

    # write some one-time information at startup
    write_dict_csv(
        fp_fp3d_csv, init_info,
        ('departureId', 'destinationId', 'tailNumber', 'flightNumber', 'time'))
    # write table headers
    write_csv(fp_status_csv, *STATUS_SCHEMA)
    write_csv(fp_fp3d_csv, *FP3D_SCHEMA)

    while (True):
        i = 0
        try:
            status = swa.get_status()
            fp3d = swa.get_fp3d_status()

            fp_fp3d_raw.write(f'{fp3d}\n')
            fp_status_raw.write(f'{init_info}\n')
            write_dict_csv(fp_status_csv, status, STATUS_SCHEMA)
            write_dict_csv(fp_fp3d_csv, fp3d, FP3D_SCHEMA)
            term_statusline('Altitude:',
                            fp3d.get('altitudeFeet'), 'feet', 'Velocity:',
                            fp3d.get('groundSpeedKnots'), 'knots', 'Pitch:',
                            fp3d.get('pitch'), 'Roll:', fp3d.get('roll'),
                            'Lat/Lon:', '(', fp3d.get('presentLat'), ',',
                            fp3d.get('presentLon'), ')', 'Heading:',
                            fp3d.get('trueHeading'), '.'
                            'ETA:', fp3d.get('estimatedTimeOfArrival'), '(',
                            status.get('pcent_flt_complete'), '%)',
                            status.get('dist_remain'), 'miles')

            i = i + 1
            if (i % 12 == 0):
                fp_fp3d_csv.flush()
                fp_fp3d_raw.flush()
                fp_status_csv.flush()
                fp_fp3d_raw.flush()
            # data does not update very often
            time.sleep(10)
        except KeyboardInterrupt:
            print()
            print('Shutting down...')
            break

    fp_fp3d_csv.close()
    fp_fp3d_raw.close()
    fp_status_csv.close()
    fp_status_raw.close()


if __name__ == '__main__':
    main()
