# Southwest Information System
Information related to the APIs available whilst onboard a Southwest flight.

- [API Docs](/sloshy/southwest-info-system/src/branch/main/Doc.md)

- [Barebones python3 Client](/sloshy/southwest-info-system/src/branch/main/py)

