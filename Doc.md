# Endpoints (and what I know about them)

Most of this information focuses on the basic SWA endpoints and
[FlightPath 3D](https://flightpath3d.com/) endpoints, which are all read-only
and primarily provide live flight data. It's definitely not exhaustive, but
still gets a bunch of cool endpoints for scraping flight data. All of these
are basic read APIs.

I have also intentionally ommitted the drink payment API and "Free Music" API.
They both involve POSTing to SWA's servers, and I don't see much value in
impersonating their website. I also have zero desire to screw with money.

I also looked for the weather APIs and didn't find them, which leads me
to believe that information is added server-side, but I could be wrong.

## Information

The base URL for all transactions is assumed to be [https://southwestwifi.com](https://southwestwifi.com),
and all transactions are assumed to be `GET`.

## /current.json

Provides current flight information (as is seen in the banner of the site).
Returns an object. Do be warned, a lot of integers are returned as strings
here for some reason.

### Schema

| Key                        | Value                | Notes                                                    |
| :------------------------- | :------------------- | :------------------------------------------------------- |
| sat_commlink_portal        | object               |                                                          |
| sat_commlink_portal/status | string               | "conn_ok" in all instances                               |
| sat_commlink_portal/time   | %a %b %d %H:%M:%S %Y | Unsure of timezone; time at the satcom station?          |
| satcomm_status             | object               |                                                          |
| satcomm_status/commlink    | string               | Whether the plane is connected; "active"                 |
| satcomm_status/linkparams  | string               | idk what this is; always seems to be "active"            |
| pcent_flt_complete         | integer, [0, 100]    |                                                          |
| altVal                     | string (feet)        | Internally an integer                                    |
| lon                        | string               | longitude                                                |
| dtzone                     | string               | destination time zone                                    |
| within_us                  | boolean              |                                                          |
| etad                       | %I:%M %p             | Estimated time at destination                            |
| lat                        | string               | latitude                                                 |
| gspdVal                    | String (knots)?      | Internally an integer; interface doesn't indicate a unit |
| ttgc                       |                      | Time until you touch down (time to ground crew?)         |
| dist_remain                | string (miles)       | Internally an integer                                    |
| actime24                   | %H:%M                | Time at destination(?)                                   |

### Example

```json
{
  "sat_commlink_portal": {
    "status": "conn_ok",
    "time": "Mon Jan 01 00:00:00 1970"
  },
  "pcent_flt_complete": 0,
  "altVal": "0",
  "lon": "0.0",
  "satcomm_status": { "commlink": "active", "linkparams": "active" },
  "dtzone": "EST",
  "within_us": true,
  "etad": "00:00 AM",
  "lat": "0.0",
  "gspdVal": "0",
  "ttgc": "0h 00m",
  "dist_remain": "0",
  "actime24": "00:00"
}
```

## /fp3d*logs/last?*={time}

Provides the current flight information from FlightPath3d. Numbers are NOT strings here.

### Parameters

- `time` [Optional] The current time, as the number of ms elapsed since the unix epoch.
  See [Date.now](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/now). - When blank, this defaults to the current time (?)

### Schema

| Key                                | Value              | Notes                                                                                     |
| :--------------------------------- | :----------------- | :---------------------------------------------------------------------------------------- |
| altitudeFeet                       | integer            |                                                                                           |
| departBaggageId                    | string             | [IATA Airport Code](https://en.wikipedia.org/wiki/IATA_airport_code)?                     |
| departureId                        | string             | [ICAO Airport Code](https://en.wikipedia.org/wiki/ICAO_airport_code)?                     |
| departureLat                       | float              |                                                                                           |
| departureLon                       | float              |                                                                                           |
| destBaggageId                      | string             | [IATA Airport Code](https://en.wikipedia.org/wiki/IATA_airport_code)?                     |
| destinationId                      | string             | [ICAO Airport Code](https://en.wikipedia.org/wiki/ICAO_airport_code)? began with K        |
| destinationLat                     | float              |                                                                                           |
| destinationLon                     | float              |                                                                                           |
| distanceToDestinationNauticalMiles | integer            |                                                                                           |
| estimatedTimeOfArrival             | integer            | I have no idea what this is                                                               |
| flightNumber                       | string             | Integer; SWA's internal flight numbering [](https://www.southwest.com/air/flight-status/) |
| fltRev                             | integer            | no clue; value was 6 on a 737 MAX8, so it's probably not revision number                  |
| groundSpeedKnots                   | integer            |                                                                                           |
| pitch                              | float or null      | This was always 0; occasionally ommitted. Unused on SWA's 737s?                           |
| positionValid                      | boolean            | ?                                                                                         |
| presentLat                         | float              | the plane's position                                                                      |
| presentLon                         | float              |                                                                                           |
| presentPhase                       | string             | "Cruise" or "Descending"                                                                  |
| roll                               | float or null      | This was always 0, occasionally ommitted. Unused on SWA's 737s?                           |
| tailNumber                         | string             | FAA N-number                                                                              |
| time                               | integer            | time of data observation (epoch _seconds_)                                                |
| timeSinceTakeoff                   | integer (minutes)  |                                                                                           |
| timeToDestination                  | integer (minutes)  |                                                                                           |
| trueHeading                        | integer (degrees)? |                                                                                           |
| weightOnWheels                     | integer            | Always zero; likely unused for SWA?                                                       |

### Example

```json
{
  "altitudeFeet": 0,
  "departBaggageId": "",
  "departureId": "",
  "departureLat": 0.0,
  "departureLon": -0.0,
  "destBaggageId": "",
  "destinationId": "",
  "destinationLat": 0.0,
  "destinationLon": -0.0,
  "distanceToDestinationNauticalMiles": 0,
  "estimatedTimeOfArrival": 0,
  "flightNumber": "0",
  "fltRev": 0,
  "groundSpeedKnots": 0,
  "pitch": 0.0,
  "positionValid": true,
  "presentLat": 0.0,
  "presentLon": -0.0,
  "presentPhase": "Cruise",
  "roll": 0.0,
  "tailNumber": "",
  "time": 0,
  "timeSinceTakeoff": 0,
  "timeToDestination": 0,
  "totalFlightTime": 0,
  "trueHeading": 0,
  "weightOnWheels": 0
}
```

## /fp3d_fcgi/project116/v2/geotags

Returns an array of objects representing popular destinations/"tags" which appear on the map.

| Key        | Value   | Notes                                |
| :--------- | :------ | :----------------------------------- |
| hashtags   | array   | Categories of activities; see `pois` |
| id         | integer |                                      |
| lat        | float   |                                      |
| lon        | float   |                                      |
| name       | string  | name of geotag/city                  |
| popular    | boolean | ?                                    |
| random_poi | object  | see `pois`                           |
| region     | string  | ?; was blank                         |
| shortcut   | string  | name but lowercase                   |

### Example

```json
[
  {
    "hashtags": [
      {
        "id": 0,
        "name": "",
        "random_poi": {
          "id": 0,
          "video_preview": {
            "id": 0,
            "preview_url": "fp3d-media-v0x/video_preview/preview_0.jpeg"
          }
        },
        "subtitle": "",
        "title": ""
      }
    ],
    "id": 0,
    "lat": 0.0,
    "lon": -0.0,
    "name": "",
    "pois_count": 0,
    "popular": false,
    "random_poi": {
      "id": 0,
      "video_preview": {
        "id": 0,
        "preview_url": "fp3d-media-v0x/video_preview/preview_0.jpeg"
      }
    },
    "region": "",
    "shortcut": ""
  }
]
```

## /fp3d_fcgi/project116/v2/pois?geotagId={id}

Returns an object containing an array of information about a specific geotag

### Parameters

- `id` is the geotag id as found by `.../geotags`

### Example

```json
{
  "items": [
    {
      "audio_url": "",
      "city": "",
      "duration": 0,
      "duration_max": 0,
      "duration_min": 0,
      "hashtags": [
        {
          "id": 0,
          "name": "",
          "subtitle": "",
          "title": ""
        }
      ],
      "id": 0,
      "lat": 0.0,
      "lon": -0.0,
      "name": "",
      "price": 0.0,
      "priority": 0,
      "ranking": 0.0,
      "rating": 0.0,
      "videodata": [
        {
          "id": 0,
          "preview_url": "fp3d-media-v0x/video_preview/preview_0.jpeg",
          "priority": 0,
          "type": "slide",
          "uri": "fp3d-media-v0x/video/video_0.mp4"
        }
      ]
    }
  ]
}
```

## /fp3d_fcgi/project116/v2/advertisement?geotagIds={id}

Returns an advertisement for a specific geotag, if one exists. Otherwise
returns null.

### Parameters

- `id` is the geotag id as found by `.../geotags`

## /fp3d_fcgi/project116/v2/geotags/nearby?limit={limit}&point={point}&range={range}

Returns geotags near to a set of coordinates

### Parameters

- `limit` is the max number of tags to return (?)
- `point` A set of coordinates in the form `lat.lat,long.long`. No space.
- `range` Integer. Miles?

## /epg.json

Returns an array of objects. Seems to serve the Live TV schedule.
I'm not sure if "endTimeEST" holds for every flight, or is just for this specific
flight because I was landing in the EST time zone.

### Example

```json
{
  "title": "",
  "started": false,
  "startTimeFloor": "0:00 am",
  "channelId": "",
  "startTimeEST": "0:00 am",
  "duration": 0,
  "endTimeEST": "0:00 am"
}
```
